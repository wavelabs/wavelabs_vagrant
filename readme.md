# The Vagrant Development VM
Below are the installation steps needed to setup this Vagrant VM. They are to be completed only 1 time.


## VirtualBox and Vagrant
Make sure you have both [VirtualBox 5.0.x](https://www.virtualbox.org) and [Vagrant 1.8.x](http://vagrantup.com) installed.

* [VirtualBox 5.0.14 for OS X hosts](http://download.virtualbox.org/virtualbox/5.0.14/VirtualBox-5.0.14-105127-OSX.dmg)
* [Vagrant 1.8.1 Mac OS X](https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1.dmg)


## Clone repository
The `wavelabs_vagrant` repository uses git submodules to pull in some upstream dependencies, such as the RVM Puppet module.

```bash
git clone https://gitlab.com/wavelabs/wavelabs_vagrant.git
cd wavelabs_vagrant
```

## Starting and Provisioning the VM
Starting the VM for the first time will also trigger a provision.
**This step takes 45 minutes to complete at a minimum**

It is recommended to not do anything else on your host machine, and have sufficient free RAM as to avoid using swap. Your time may vary but its recommended to walk away and grab a coffee or lunch at this point.

```bash
vagrant up --provision
```

***Congrats, you have set up 95% of the dev environment.***

## Connecting to the VM
Vagrant provides a simple command to connect to the VM and get a shell :

```bash
vagrant ssh
```

## Running servers and forwarded ports
Since the VM is intended to mimic our Wavelabs API Servers it runs almost all the same servers such as IDN Oauth API Server, Ruby on Rails Module API Server. In order to access these servers and services more easily we have instructed Vagrant to forward local ports to the VM. You can see the full list of these servers and services, including which port numbers are being forwarded, in this repo's [Vagrantfile](Vagrantfile) file found at the root of the repository.

## Running Ruby On Rails Module API server

```bash
$ vagrant ssh
$ cd /app/code/idn-api-rails
$ rails server -b 0.0.0.0
```
 After running the above commands rails API Server will run on port 3000. You can test the API using any REST client using 'http://localhost:3000'
