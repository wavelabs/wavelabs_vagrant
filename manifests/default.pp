node wavelabs {
  # NOTE : this loads in the common wavelabs platform requirements like Oracle, Mongo and RabbitMQ
  include rubyrails::common

  # NOTE : Load the common class before the core servers are installed, mainly to get the apt-get update
  Exec["common-apt-update"] -> Service["redis-server"]
  Exec["common-apt-update"] -> Service["mysql"]
  Exec { path =>  [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }


  include rubyrails::rails_dev_setup
  include apache::http
  include php::php_dev_setup

}

# NOTE : to manually run all the Puppet classes run the following 2 bash commands in a vagrant ssh shell :
#   sudo su -
#   cd /vagrant/manifests && puppet apply --verbose --trace --debug --modulepath '/etc/puppet/modules:/vagrant/modules' default.pp --detailed-exitcodes
