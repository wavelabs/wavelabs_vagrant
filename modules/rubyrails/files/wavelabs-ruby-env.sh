export rvm_always_trust_rvmrc_flag=1
export rvm_trust_rvmrcs_flag=1
export rvm_verify_downloads_flag=1

# Rails configuration
export RAILS_ENV=vagrant

# Rack configuration
export RACK_ENV=vagrant
