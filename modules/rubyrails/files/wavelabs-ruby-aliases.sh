# General aliases
alias tails="tail -f -n1000"
alias tsys="tails /var/log/syslog"

# Rails alias stuff
alias sc="./script/rails console"
alias trd="tail -f -n400 log/development.log"
alias trf="tail -f -n400 log/facebook_development.log"
alias trt="tail -f -n400 log/test.log"
alias be="bundle exec"

# Git alias stuff
alias gss="git stash save"
alias gsp="git stash pop"
alias gsl="git stash list"
alias gb="git branch"
alias gba="git branch -a"
