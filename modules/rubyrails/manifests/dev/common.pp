class rubyrails::dev::common {

  include rvm

  package { 'git-core': ensure => present }

  if !defined(Rvm_system_ruby['ruby-2.2.3']) {
    rvm_system_ruby { 'ruby-2.2.3':
      ensure      => 'present',
      default_use => true
    }
  }

  # Add vagrant to the www-group so capistrano can work without permission issues when cloning the git repo on deploy
  exec { "add vagrant to the www-data group":
    command => "/usr/sbin/usermod -a -G www-data vagrant",
    unless  => "/bin/cat /etc/group | grep www-data | grep vagrant";
  }
}
