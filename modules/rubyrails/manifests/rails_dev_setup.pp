class rubyrails::rails_dev_setup() {
  include rubyrails::common
  include rubyrails::dev::common

  ensure_packages(['libgmp3-dev'])

  file { "/etc/sudoers.d/vagrant":
    source => "puppet:///modules/rubyrails/vagrant_sudoer",
    mode   => 0440,
    owner  => "root",
    group  => "root"
  }


  # Setup the directory which will house all the repos we want to clone in this VM
  file { "/app/code/":
    owner  => "vagrant",
    group  => "vagrant",
    ensure => directory,
    mode   => 755
  }

  if !defined(Rvm_system_ruby["ruby-2.2.3"]) {
    rvm_system_ruby { "ruby-2.2.3":
      ensure      => "present",
      default_use => true
    }
  }

  if !defined(Rvm_gemset["ruby-2.2.3@global"]) {
    rvm_gemset { "ruby-2.2.3@global":
      ensure  => present,
      require => Rvm_system_ruby["ruby-2.2.3"]
    }
  }

  # install a particular version of bundler (since we can't seem to do this through the RVM ruby install process)
  rvm_gem { "ruby-2.2.3@global/bundler":
    ensure  => "1.13.6",
    require => Rvm_gemset["ruby-2.2.3@global"]
  }

  # Setup the idn-api-rails repo to test
  exec { "clone idn-api-rails repo":
    require => [Package["git-core"], File["/app/code/"]],
    command => "/usr/bin/git clone https://gitlab.com/wavelabs/idn-api-rails.git",
    user    => "vagrant",
    cwd     => "/app/code/",
    timeout => 300,
    unless  => "/usr/bin/test -d /app/code/idn-api-rails"
  }
  exec { "bundle install idn-api-rails":
    command => "/bin/bash --login -c 'rvm info && pwd && bundle install'",
    user    => "vagrant",
    cwd     => "/app/code/idn-api-rails",
    timeout => 1200,
    # logoutput => true,
    require => [
      Rvm_system_ruby['ruby-2.2.3'],
      Rvm_gem["ruby-2.2.3@global/bundler"],
      Exec["clone idn-api-rails repo"]
    ]
  }

  exec { "Create idn-api-rails development db & migrate":
    command => "/bin/bash --login -c 'rvm info && pwd && bundle exec rake db:migrate'",
    user    => "vagrant",
    cwd     => "/app/code/idn-api-rails",
    require => [
      Rvm_system_ruby['ruby-2.2.3'],
      Rvm_gem["ruby-2.2.3@global/bundler"],
      Exec["bundle install idn-api-rails"]
    ]

  }

}

Class["rubyrails::common"] -> Class["rubyrails::rails_dev_setup"]

# apply individual puppet command:
# sudo puppet apply --verbose --debug --modulepath=/vagrant/modules -e 'class { "rubyrails::rails_dev_setup": }'
