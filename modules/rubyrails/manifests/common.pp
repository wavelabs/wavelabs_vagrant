class rubyrails::common {
  # NOTE : mainly needed in order to run this class standalone

  include redis

  #include apache

  file { '/etc/rvmrc':
    source => "puppet:///modules/rubyrails/rvmrc",
    mode   => '0664',
    owner  => 'root',
    group  => "rvm",
    before => Class['rvm']
  }

  # Install a specific/tagged version RVM
  class { "rvm":
    version      => '1.26.10',
    manage_rvmrc => false
  }
  # class { "rvm": version => 'master' } # Install the master branch of the RVM github repo
  rvm::system_user { "vagrant": }

  # NOTE : needed for the "curb" Ruby gem
  if !defined(Package['libcurl4-openssl-dev'])  { package { 'libcurl4-openssl-dev': ensure => installed } }


  # Install and configure MySQL
  class { 'mysql::server':
    root_password => 'root',
    restart => true,
    override_options => { 'mysqld' => { 'bind_address' => '0.0.0.0' } },
    grants => {
      'root@%/*.*' => {
        ensure     => 'present',
        options    => ['GRANT'],
        privileges => ['ALL'],
        table      => '*.*',
        user       => 'root@%'
      },
    }
  }

  mysql::db { 'development':
    user     => 'development',
    password => 'development',
    host     => '%',
    grant    => ['ALL']
  }

  mysql::db { 'idn-api-rails_development':
    user     => 'root',
    password => 'root',
    host     => '%',
    grant    => ['ALL']
  }

  mysql::db { 'test':
    user     => 'test',
    password => 'test',
    host     => '%',
    grant    => ['ALL']
  }

  include mysql::client

  class { 'mysql::bindings':
    client_dev          => true,
    ruby_enable         => true,
    ruby_package_ensure => "present"
  }


  file { "/etc/bash.bashrc":
    source => "puppet:///modules/rubyrails/bash.bashrc",
    owner  => root,
    group  => root;
  }


  file { "/app/":
    owner  => "vagrant",
    group  => "vagrant",
    ensure => directory,
    mode   => 755
  }

}
