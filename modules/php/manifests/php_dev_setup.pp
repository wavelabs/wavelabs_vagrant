class php::php_dev_setup() {

  package { "php5":
    ensure => present,
  }

  package { "php5-cli":
    ensure => present,
  }

  package { "php5-xdebug":
    ensure => present,
  }

  package { "php5-mysql":
    ensure => present,
  }

  package { "php5-imagick":
    ensure => present,
  }

  package { "php5-mcrypt":
    ensure => present,
  }

  package { "php-pear":
    ensure => present,
  }

  package { "php5-dev":
    ensure => present,
  }

  package { "php5-curl":
    ensure => present,
  }

  package { "php5-sqlite":
    ensure => present,
  }

  package { "libapache2-mod-php5":
    ensure => present,
  }

  mysql::db { 'nbos_module_todo':
    user     => 'root',
    password => 'root',
    host     => '%',
    grant    => ['ALL']
  }


  # Setup the idn-api-rails repo to test
  exec { "clone idn-api-php repo":
    require => [Package["git-core"], File["/app/code/"]],
    command => "/usr/bin/git clone https://gitlab.com/wavelabs/idn-api-php.git",
    user    => "vagrant",
    cwd     => "/app/code/",
    timeout => 300,
    unless  => "/usr/bin/test -d /app/code/idn-api-php"
  }

  exec { "Create idn-api-php development db & migrate":
    require => [Service["mysqld"], Mysql::Db["nbos_module_todo"], Exec["clone idn-api-php repo"]],
    user    => "vagrant",
    cwd => "/app/code/idn-api-php/application/data/",
    command => "/usr/bin/mysql -u root -p root -D nbos_module_todo < nbos_module_todo.sql"
  }

  #exec { "Get idn php library using composer ":
    #require => [Package["php5-cli"], Exec["clone idn-api-php repo"]],
    #user    => "vagrant",
    #cwd => "/app/code/idn-api-php/",
   # command => "/usr/bin/php composer.phar install"
  #}

  # Setups the virtual host
  file { '/etc/apache2/sites-enabled/site.conf':
    source => "puppet:///modules/php/site.conf",
    mode   => 0440,
    owner  => "root",
    group  => "root",
    notify  => Service['apache2'],
    require => Package['apache2'],
  }

}