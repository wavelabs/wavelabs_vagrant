class redis {
  require common
  package { "redis-server":
    ensure  => present;
  } ->
  file { "/etc/redis/redis.conf":
    source => "puppet:///modules/redis/redis.conf",
    mode   => 0440,
    owner  => "redis",
    group  => "redis"
  }
  service { "redis-server":
    ensure  => running,
    require => [Package["redis-server"],File["/etc/redis/redis.conf"]];
  }
}
