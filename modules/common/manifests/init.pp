class common {
  exec { "common-apt-update":
    command => "/usr/bin/apt-get -y update",
    timeout => 3600;
  }

  if !defined(Package['unzip']) {
    package { "unzip": ensure => installed }
    Exec["common-apt-update"] -> Package["unzip"]
  }

  package { 'libssl-dev': ensure => present }
  package { 'openssl': ensure => present }
  #package { 'maven': ensure => present }

  file { "/etc/hosts":
    ensure => present
  }


  # Setup locale en_US.UTF-8 (<region>.<charmap>) as default.
  # Credit for this approach goes to :
  # http://blog.toxa.de/archives/535
  file { "/etc/default/locale":
    content => "LANG=en_US.UTF-8"
  }
  exec { "set default locale to en_US.UTF-8":
    command     => "/usr/sbin/update-locale",
    user        => "root",
    require     => File["/etc/default/locale"],
    environment => [
      "LANG=en_US.UTF-8",
      "LANGUAGE=en_US.UTF-8"
    ]
  }

  # NOTE : leaving this comment in for now until i have some more condifence in the puppet setup
  # Exec["common-apt-update"] -> Package <| |>
}

# apply individual puppet command:
# sudo puppet apply --verbose --debug --modulepath=/vagrant/modules -e 'class { "common": }'
